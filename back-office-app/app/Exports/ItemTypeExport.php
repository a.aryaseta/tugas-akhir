<?php

namespace App\Exports;

use App\Models\Itemtypes;
use Maatwebsite\Excel\Concerns\FromCollection;

class ItemTypeExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Itemtypes::all();
    }
}
