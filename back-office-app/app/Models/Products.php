<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    protected $fillable = ['name', 'photo', 'description', 'price', 'stock', 'itemtype_id'];
    protected $with = array('itemtypes');

    public function itemtypes()
    {
        return $this->belongsTo(Itemtypes::class, 'itemtype_id', 'id');
    }
}
