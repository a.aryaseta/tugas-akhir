<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Itemtypes extends Model
{
    protected $table = "itemtypes";
    protected $fillable = ['name', 'category_id', 'brand_id'];
}
