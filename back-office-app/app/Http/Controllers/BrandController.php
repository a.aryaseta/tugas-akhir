<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brands;
use Alert;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Exports\BrandsExport;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $brands = new Brands;
        if($request->search){
            $brands = $brands->where('name','like', '%'.$request->search.'%');
        }
        $items = $brands->paginate(5);
        return view('brands.index', ['items' => $items, 'search' => $request->search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brands::all();
        return view('brands.create', ['brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $brands = new Brands;
        $brands->name = $request->name;
        $brands->save();
        Alert::success('Congrats', 'Brands Succesfully Saved');
        return redirect('/admin/brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Brands::find($id);
        return view('brands.edit', ['brands' => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $brands = Brands::find($id);
        $brands->name = $request->name;
        $brands->update();
        Alert::success('Congrats', 'Brands Succesfully Updated');
        return redirect('/admin/brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brands = Brands::find($id);
        $brands->delete();
        Alert::info('Congrats', 'Item Type Succesfully Deleted');
        return redirect('/admin/brands');
    }

    public function fileExport()
    {
        return Excel::download(new BrandsExport, 'brands-collection.xlsx');
    }

    public function pdf()
    {
    	$items = Brands::all();
    	$pdf = PDF::loadview('brands.pdf',['items'=>$items]);
    	return $pdf->download('brands.pdf');
    }
}
