<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Itemtypes;
use App\Models\Categories;
use App\Models\Brands;
use Alert;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Exports\ItemTypeExport;

class ItemTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemtypes = new Itemtypes;
        if($request->search){
            $itemtypes = $itemtypes->where('name','like', '%'.$request->search.'%');
        }
        $items = $itemtypes->paginate(5);
        return view('itemtypes.index', ['items' => $items, 'search' => $request->search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itemtypes = Itemtypes::all();
        $categories = Categories::all();
        $brands = Brands::all();
        return view('itemtypes.create', ['itemtypes' => $itemtypes, 'categories' => $categories, 'brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required'
        ]);

        $itemtypes = new Itemtypes;
        $itemtypes->name = $request->name;
        $itemtypes->category_id = $request->category_id;
        $itemtypes->brand_id = $request->brand_id;
        $itemtypes->save();
        Alert::success('Congrats', 'Item Type Succesfully Saved');
        return redirect(route('itemtypes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemtypes = Itemtypes::find($id);
        $categories = Categories::all();
        $brands = Brands::all();
        return view('itemtypes.edit', ['itemtypes' => $itemtypes, 'categories' => $categories, 'brands' => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required'
        ]);

        $itemtypes = Itemtypes::find($id);
        $itemtypes->name = $request->name;
        $itemtypes->category_id = $request->category_id;
        $itemtypes->brand_id = $request->brand_id;
        $itemtypes->update();
        Alert::success('Congrats', 'Item Type Succesfully Updated');
        return redirect(route('itemtypes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Itemtypes::find($id);
        $item->delete();
        Alert::info('Congrats', 'Item Type Succesfully Deleted');
        return redirect(route('itemtypes.index'));
    }

    public function fileExport()
    {
        return Excel::download(new ItemTypeExport, 'itemtypes-collection.xlsx');
    }

    public function pdf()
    {
    	$items = Itemtypes::all();
    	$pdf = PDF::loadview('itemtypes.pdf',['items'=>$items]);
    	return $pdf->download('itemtypes.pdf');
    }

}
