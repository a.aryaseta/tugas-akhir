<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use Alert;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Exports\CategoriesExport;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $categories = new Categories;
        if($request->search){
            $categories = $categories->where('name','like', '%'.$request->search.'%');
        }
        $items = $categories->paginate(5);
        return view('categories.index', ['items' => $items, 'search' => $request->search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        return view('categories.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $categories = new Categories;
        $categories->name = $request->name;
        $categories->save();
        Alert::success('Congrats', 'Categories Succesfully Saved');
        return redirect('/admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::find($id);
        return view('categories.edit', ['categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $categories = Categories::find($id);
        $categories->name = $request->name;
        $categories->update();
        Alert::success('Congrats', 'Categories Succesfully Updated');
        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Categories::find($id);
        $categories->delete();
        Alert::info('Congrats', 'Categories Succesfully Deleted');
        return redirect('/admin/categories');
    }

    public function fileExport()
    {
        return Excel::download(new CategoriesExport, 'categories-collection.xlsx');
    }

    public function pdf()
    {
    	$items = Categories::all();
    	$pdf = PDF::loadview('categories.pdf',['items'=>$items]);
    	return $pdf->download('categories.pdf');
    }
}
