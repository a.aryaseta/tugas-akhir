<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Itemtypes;

class CatalogController extends Controller
{
    public function index(){
        $products = Products::all();
        return view('catalog.index', ['products' => $products]);
    }

    public function detail($id){
        $product = Products::find($id);
        $item = Itemtypes::where('id', $product->itemtype_id)->first();
        return view('catalog.detail.index', ['product' => $product, 'item' => $item
    ]);
    }
}
