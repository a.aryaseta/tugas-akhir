<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Itemtypes;
use App\Models\Categories;
use App\Models\Brands;
use File;
use Alert;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Exports\ProductsExport;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $products = new Products;
        $products = Products::with('itemtypes')->paginate(5);
        if($request->search){
            $products = Products::with('itemtypes')->where('name','like', '%'.$request->search.'%')->orWhere('description','like', '%'.$request->search.'%')->paginate(5);
        }
        return view('products.index', ['products' => $products, 'search' => $request->search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Products::all();
        $itemtypes = Itemtypes::all();
        $categories = Categories::all();
        $brands = Brands::all();
        return view('products.create', ['products' => $products, 'itemtypes' => $itemtypes, 'categories' => $categories, 'brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'name_photo' => 'required',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'itemtype_id' => 'required'
        ]);

        $products = new Products;
        $products->name = $request->name;
        $products->description = $request->description;
        $products->price = $request->price;
        $products->photo = $request->photo;
        $products->stock = $request->stock;
        $products->itemtype_id = $request->itemtype_id;
        $name_photo = $request->name_photo;
        $photo = time().'.'.$name_photo->getClientOriginalExtension();
        $name_photo->move('images/', $photo);
        $products->photo = $photo;
        $products->save();
        Alert::success('Congrats', 'Product Succesfully Saved');
        return redirect(route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Products::find($id);
        $itemtypes = Itemtypes::all();
        $categories = Categories::all();
        $brands = Brands::all();
        return view('products.edit', ['products' => $products, 'itemtypes' => $itemtypes, 'categories' => $categories, 'brands' => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'name_photo' => 'required',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'itemtype_id' => 'required'
        ]);

        $products = Products::find($id);
        $products->name = $request->name;
        $products->description = $request->description;
        $products->price = $request->price;
        $products->photo = $request->photo;
        $products->stock = $request->stock;
        $products->itemtype_id = $request->itemtype_id;
        $name_photo = $request->name_photo;
        $photo = time().'.'.$name_photo->getClientOriginalExtension();
        $name_photo->move('images/', $photo);
        $products->photo = $photo;
        $products->update();
        Alert::success('Congrats', 'Product Succesfully Updated');
        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Products::find($id);
        $item->delete();
        Alert::info('Congrats', 'Product Succesfully Deleted');
        return redirect(route('products.index'));
    }

    public function fileExport()
    {
        return Excel::download(new ProductsExport, 'products-collection.xlsx');
    }

    public function pdf()
    {
    	$items = Products::all();
    	$pdf = PDF::loadview('products.pdf',['items'=>$items]);
    	return $pdf->download('products.pdf');
    }
}
