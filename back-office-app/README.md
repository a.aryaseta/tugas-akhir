<h1>Final Project</h1>
<h2>Kelompok 23</h2><hr><br>
<h3>Judul Project: Catalog Thrifting</h3><br>
<h3>Anggota:</h3>
<ol>
<li>Aulia Aryaseta</li>
<li>Muhammad Zaki 'Ammar</li>
<li>Nasywa Sakha Ningrum</li>
</ol><br><hr>
<h1>ERD</h1>
<p align="center"><img src="ERDfinalProject.png" width="400"></p><br><br>
<h2>Link</h2>
<ul>
<li>Link Demo Aplikasi : https://drive.google.com/file/d/1fXsVIQmJzbU_66t_lTm1tQVjPXG1z-sL/view?usp=sharing </li>
<li>Link Deploy : https://catalog-thrifting.abera.id/ </li>
</ul>