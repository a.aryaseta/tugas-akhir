<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ItemTypesController;

Auth::routes();
Route::get('/', 'CatalogController@index')->name('catalog.index');
Route::get('/catalog/{id}/detail', 'CatalogController@detail')->name('catalog.detail');

Route::group(['middleware' => ['auth']], function(){
    Route::prefix('admin')->group(function () {
        Route::resource('dashboard', 'DashboardController');
        Route::resource('brands', 'BrandController');
        Route::resource('categories', 'CategoriesController');
        Route::resource('products', 'ProductsController');
        Route::resource('itemtypes', 'ItemTypesController');
        Route::get('export/itemtypes', 'ItemTypesController@fileExport')->name('itemtypes-export');
        Route::get('pdf/itemtypes', 'ItemTypesController@pdf')->name('itemtypes-pdf');
        Route::get('export/brands', 'BrandController@fileExport')->name('brands-export');
        Route::get('pdf/brands', 'BrandController@pdf')->name('brands-pdf');
        Route::get('export/categories', 'CategoriesController@fileExport')->name('categories-export');
        Route::get('pdf/categories', 'CategoriesController@pdf')->name('categories-pdf');
        Route::get('export/products', 'ProductsController@fileExport')->name('products-export');
        Route::get('pdf/products', 'ProductsController@pdf')->name('products-pdf');
        Route::resource('profile', 'ProfileController');
    });
});
Route::get('/admin', function () {
    return view('auth.login');
});

