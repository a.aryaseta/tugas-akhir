@extends('catalog.layouts.master')

@section('content')
<section class="page-section bg-light" id="catalog">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">catalog</h2>
            <h3 class="section-subheading text-muted">Finding your product dream in here !</h3>
        </div>
        <div class="row">
            @include('catalog.list.index')
        </div>
    </div>
</section>
@endsection
