@extends('catalog.layouts.detail')
@section('name')
{{ $product->name }}
@endsection
@section('content')
<section class="page-section bg-light" id="catalog">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Product Detail</h2>
            <h3 class="section-subheading text-muted">We Hope it's your dream !</h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="modal-body">
                    <h2 class="text-uppercase">{{ $product->name }}</h2>
                    <p class="item-intro text-muted">IDR {{ number_format($product->price) }}</p>
                    <img class="img-fluid d-block mx-auto" src="{{asset('images/'.$product->photo)}}" alt="..." />
                    <ul class="list-inline mt-4">
                        <li class="mb-2">
                            <strong>Stock:</strong>
                            {{ $product->stock }}
                        </li>
                        <li class="mb-2">
                            <strong>Category:</strong>
                            {{ \App\Models\Categories::where('id', $item->category_id)->value('name') }}
                        </li>
                        <li>
                            <strong>Brand:</strong>
                            {{ \App\Models\Brands::where('id', $item->brand_id)->value('name') }}
                        </li>
                    </ul>
                    <p class="mt-4">{{ $product->description }}</p>
                    <a href="{{ URL::previous() }}" style="width: 100%;" class="btn btn-primary btn-xl text-uppercase mt-4">
                        <i class="fas fa-arrow-left me-1"></i>
                        Back
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
