@foreach($products as $product)
<div class="col-lg-4 col-sm-6 mb-4">
        <div class="catalog-item">
            <a class="catalog-link" href="{{route('catalog.detail', $product->id)}}">
                <div class="catalog-hover">
                    <div class="catalog-hover-content"><i class="fas fa-eye fa-3x"></i></div>
                </div>
                <img class="img-fluid" src="{{asset('images/'.$product->photo)}}" alt="..." />
            </a>
            <div class="catalog-caption">
                <div class="catalog-caption-heading">{{ $product->name }}</div>
                <div class="catalog-caption-subheading text-muted">IDR {{ number_format($product->price) }}</div>
            </div>
        </div>
    </div>
@endforeach
