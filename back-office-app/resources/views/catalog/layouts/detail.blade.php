<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>@yield('name')</title>
        <link rel="icon" type="image/x-icon" href="{{asset('template-front/assets/favicon.ico')}}" />
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <link href="{{asset('template-front/css/styles.css')}}" rel="stylesheet" />
    </head>
    <body id="page-top">
        @include('catalog.layouts.navbar')
        @yield('content')
        @include('catalog.layouts.footer')
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{asset('template-front/js/scripts.js')}}"></script>
    </body>
</html>
