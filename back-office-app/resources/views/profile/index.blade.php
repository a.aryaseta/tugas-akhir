@extends('layouts.master')

@section('title')
Profile Page
@endsection

@section('content')
<a href="{{URL::previous()}}" class="btn btn-primary mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form method="post" action="{{route('profile.update', $profile->id)}}">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" value="{{ $profile->username }}">
          </div>
          @error('username')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="form-group">
            <label for="phone">Phone</label>
            <input type="number" class="form-control" id="phone" name="phone" value="{{ $profile->phone }}">
          </div>
          @error('phone')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="form-group">
            <label for="gender">Gender</label>
            <select name="gender" id="gender" class="form-control text-capitalize">
                <option value="{{ $profile->gender }}" selected>{{ $profile->gender }}</option>
                @if($profile->gender != 'man')
                <option value="man">Man</option>
                @else
                <option value="woman">Woman</option>
                @endif
            </select>
            @error('gender')
                <div class="alert alert-danger">
                <strong>{{ $message }}</strong>
                </div>
            @enderror
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <textarea name="address" id="address" cols="30" rows="10" class="form-control">{{ $profile->address }}</textarea>
                                @error('address')
                                    <div class="alert alert-danger">
                                    <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
            @error('address')
                <div class="alert alert-danger">
                <strong>{{ $message }}</strong>
                </div>
            @enderror
          </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
          </div>
          @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="form-group">
            <label for="password">New Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Fill if you want to change password">
          </div>
          @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="{{URL::previous()}}" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
