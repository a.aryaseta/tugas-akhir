@extends('layouts.master')

@section('title')
Brand Page
@endsection

@section('content')
<a href="{{URL::previous()}}" class="btn btn-primary mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form method="post" action="{{route('brands.store')}}">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="{{URL::previous()}}" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection