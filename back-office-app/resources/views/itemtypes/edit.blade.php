@extends('layouts.master')

@section('title')
Item Types Page
@endsection

@section('content')
<a href="{{URL::previous()}}" class="btn btn-primary mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form method="post" action="{{route('itemtypes.update', $itemtypes->id)}}">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$itemtypes->name}}">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="name">Category</label>
          <select name="category_id" id="category_id" class="form-control">
            @foreach($categories as $category)
            @if($category->id == $itemtypes->category_id)
            <option value="{{ $itemtypes->category_id }}" selected>{{ App\Models\Categories::where('id',$itemtypes->category_id)->value('name') }}</option>
            @else
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endif
            @endforeach
          </select>
          @error('category_id')
              <div class="alert alert-danger">
              <strong>{{ $message }}</strong>
              </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="name">Brand</label>
          <select name="brand_id" id="brand_id" class="form-control">
            @foreach($brands as $brand)
            @if($brand->id == $itemtypes->brand_id)
            <option value="{{ $itemtypes->brand_id }}" selected>{{ App\Models\Brands::where('id',$itemtypes->brand_id)->value('name') }}</option>
            @else
            <option value="{{$brand->id}}">{{$brand->name}}</option>
            @endif
            @endforeach
          </select>
          @error('brand_id')
              <div class="alert alert-danger">
              <strong>{{ $message }}</strong>
              </div>
          @enderror
        </div>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="{{URL::previous()}}" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
