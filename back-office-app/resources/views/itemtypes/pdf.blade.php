<!DOCTYPE html>
<html>
<head>
	<title>Item Types</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>

	<table class='table table-bordered'>
		<thead>
			<tr>
                <th>No</th>
                <th>Item Name</th>
                <th>Categories Name</th>
                <th>Brand Name</th>
            </tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
            @foreach($items as $key => $item)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{ $item->name }}</td>
					<td>{{ App\Models\Categories::where('id', $item->category_id)->value('name') }}</td>
					<td>{{ App\Models\Brands::where('id', $item->brand_id)->value('name') }}</td>
                </tr>
            @endforeach
		</tbody>
	</table>

</body>
</html>
