@extends('layouts.master')

@section('title')
Item Types Page
@endsection

@section('content')
<a href="{{URL::previous()}}" class="btn btn-primary mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form method="post" action="{{route('itemtypes.store')}}">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="name">Category</label>
          <select name="category_id" id="category_id" class="form-control">
            <option selected>Choose...</option>
            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
          @error('category_id')
              <div class="alert alert-danger">
              <strong>{{ $message }}</strong>
              </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="name">Brand</label>
          <select name="brand_id" id="brand_id" class="form-control">
            <option selected>Choose...</option>
            @foreach($brands as $brand)
            <option value="{{$brand->id}}">{{$brand->name}}</option>
            @endforeach
          </select>
          @error('brand_id')
              <div class="alert alert-danger">
              <strong>{{ $message }}</strong>
              </div>
          @enderror
        </div>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="{{URL::previous()}}" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection