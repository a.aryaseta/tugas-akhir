@extends('layouts.master')

@section('title')
Item Type Page
@endsection

@section('content')
<div class="container-fluid">
    <form method="get" action="{{ route('itemtypes.store') }}">
        <div class="row">
            <div class="col-2">
                <a style="width: 100%;" href="{{route('itemtypes.create')}}" class="btn btn-primary btn-md mb-3"><i class="fa fa-plus" aria-hidden="true"></i> Create Data</a>
            </div>
            <div class="col-4 p-0">
                <div class="form-group">
                    <input type="text" class="form-control" id="search" name="search" placeholder="Search By Name" value={{ $search }}>
                </div>
            </div>
            <div class="col-2">
                <button style="width: 100%;" type="submit" class="btn btn-success btn-md mb-3"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
            </div>
            <div class="col-2">
                <a style="width: 100%;" href="{{route('itemtypes-export')}}" class="btn btn-warning btn-md mb-3">Export as XLS</a>
            </div>
            <div class="col-2">
                <a style="width: 100%;" href="{{route('itemtypes-pdf')}}" class="btn btn-info btn-md mb-3">Export as PDF</a>
            </div>
        </div>
    <form>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Item Type</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Item Name</th>
                            <th>Categories Name</th>
                            <th>Brand Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $key => $item)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{ $item->name }}</td>
					        <td>{{ App\Models\Categories::where('id', $item->category_id)->value('name') }}</td>
					        <td>{{ App\Models\Brands::where('id', $item->brand_id)->value('name') }}</td>
                            <td>
                                <form action="{{route('itemtypes.destroy', $item->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <a href="{{route('itemtypes.edit', $item->id)}}" class="btn btn-warning btn-sm text-white"><i class="fa fa-pen"></i></a>
                                    <button onclick="return confirm('Are you sure to delete this data?')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="float-right">
                {{ $items->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
