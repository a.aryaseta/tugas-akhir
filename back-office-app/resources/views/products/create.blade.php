@extends('layouts.master')

@section('title')
Categories Page
@endsection

@section('content')
<a href="{{URL::previous()}}" class="btn btn-primary mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form method="post" enctype="multipart/form-data" action="{{route('products.index')}}">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="name_photo">Photo</label>
            <input type="file" class="form-control" name="name_photo">
          </div>
        @error('photo')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" id="description" name="description">
          </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control" id="price" name="price">
          </div>
        @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="form-group">
            <label for="stock">Stock</label>
            <input type="text" class="form-control" id="stock" name="stock">
          </div>
        @error('stock')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="name">Item Types</label>
          <select name="itemtype_id" id="itemtype_id" class="form-control">
            <option selected>Choose...</option>
            @foreach($itemtypes as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
          </select>
          @error('brand_id')
              <div class="alert alert-danger">
              <strong>{{ $message }}</strong>
              </div>
          @enderror
        </div>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="{{URL::previous()}}" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection