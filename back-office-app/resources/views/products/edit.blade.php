@extends('layouts.master')

@section('title')
Categories Page
@endsection

@section('content')
<a href="{{URL::previous()}}" class="btn btn-primary mb-3"><i class="fa fa-arrow-left"></i> Kembali</a>
<div class="card" style="border:none;">
  <div class="card-body">
    <div class="col-12 p-0">
      <form method="post" enctype="multipart/form-data" action="{{route('products.update',$products->id)}}">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" value="{{$products->name}}" class="form-control" id="name" name="name">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="name_photo">Photo</label>
            <input type="file" value="{{$products->photo}}" class="form-control" name="name_photo">
          </div>
        @error('photo')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" value="{{$products->description}}" class="form-control" id="description" name="description">
          </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" value="{{$products->price}}" class="form-control" id="price" name="price">
          </div>
        @error('price')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="form-group">
            <label for="stock">Stock</label>
            <input type="text" value="{{$products->stock}}" class="form-control" id="stock" name="stock">
          </div>
        @error('stock')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="form-group">
            <label for="itemtype_id">Itemtype</label>
            <input type="text" value="{{$products->itemtype_id}}" class="form-control" id="itemtype_id" name="itemtype_id">
          </div>
        @error('itemtype_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="{{URL::previous()}}" class="btn btn-warning text-white">Batal</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection