<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item @if(Request::segment(2) == 'dashboard') active @endif">
        <a class="nav-link" href="{{route('dashboard.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <li class="nav-item @if(Request::segment(2) == 'product') active @endif">
        <a class="nav-link" href="{{route('products.index')}}">
            <i class="fas fa-fw fa-shopping-bag"></i>
            <span>Product</span></a>
    </li>
    <li class="nav-item @if(Request::segment(2) == 'categories') active @endif">
        <a class="nav-link" href="{{route('categories.index')}}">
            <i class="fas fa-fw fa-shopping-cart"></i>
            <span>Category</span></a>
    </li>
    <li class="nav-item @if(Request::segment(2) == 'brands') active @endif">
        <a class="nav-link" href="{{route('brands.index')}}">
            <i class="fas fa-fw fa-receipt"></i>
            <span>Brands</span></a>
    </li>
    <li class="nav-item @if(Request::segment(2) == 'itemtypes') active @endif">
        <a class="nav-link" href="{{route('itemtypes.index')}}">
            <i class="fas fa-fw fa-shopping-basket"></i>
            <span>Item Type</span></a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline mt-5">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
